# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "fabiolb/map.jinja" import fabiolb with context %}

fabiolb-remove-service:
  service.dead:
    - name: fabiolb
  file.absent:
    - name: /etc/systemd/system/fabiolb.service
    - require:
      - service: fabiolb-remove-service
  cmd.run:
    - name: 'systemctl daemon-reload'
    - onchanges:
      - file: fabiolb-remove-service

fabiolb-remove-binary:
   file.absent:
    - name: {{ fabiolb.bin_dir }}/fabiolb
    - require:
      - fabiolb-remove-service

fabiolb-remove-config:
    file.absent:
      - name: {{ fabiolb.config_dir }}
      - require:
        - fabiolb-remove-binary

fabiolb-remove-certdir:
    file.absent:
      - name: {{ fabiolb.config.cert_dir }}
      - require:
        - fabiolb-remove-config
      - onlyif:
        - 'command -v {{ fabiolb.bin_dir }}/fabiolb'

fabiolb-remove-cacertdir:
    file.absent:
      - name: {{ fabiolb.config.cacert_dir }}
      - require:
        - fabiolb-remove-config
      - onlyif:
        - 'command -v {{ fabiolb.bin_dir }}/fabiolb'
