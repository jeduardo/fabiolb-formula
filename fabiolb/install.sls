# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "fabiolb/map.jinja" import fabiolb with context %}

fabiolb-create-user:
  user.present:
    - name: {{ fabiolb.user }}
    - shell: /bin/false
    - system: True
    - home: /dev/null
    - createhome: False

fabiolb-create-group:
  group.present:
    - name: {{ fabiolb.group }}
    - members:
      - {{ fabiolb.user }}
    - require:
      - user: {{ fabiolb.user }}

fabiolb-bin-dir:
  file.directory:
    - name: {{ fabiolb.bin_dir }}
    - makedirs: True

fabiolb-dist-dir:
  file.directory:
    - name: {{ fabiolb.dist_dir }}
    - makedirs: True

fabiolb-config-dir:
  file.directory:
    - name: {{ fabiolb.config_dir }}
    - makedirs: True
    - user: {{ fabiolb.user }}
    - group: {{ fabiolb.group }}
    - mode: 0750
    - require:
      - user: {{ fabiolb.user }}
      - group: {{ fabiolb.group }}

fabiolb-install-binary:
  file.managed:
    - name: {{ fabiolb.dist_dir }}/fabio-{{ fabiolb.version }}
    - source: https://github.com/fabiolb/fabio/releases/download/v{{ fabiolb.version }}/fabio-{{ fabiolb.version }}-go{{ fabiolb.go_runtime }}-{{ grains['kernel'] | lower }}_{{ fabiolb.arch }}
    - source_hash: https://github.com/fabiolb/fabio/releases/download/v{{ fabiolb.version }}/fabio-{{ fabiolb.version }}-go{{ fabiolb.go_runtime }}.sha256
    - user: {{ fabiolb.user }}
    - group: {{ fabiolb.group }}
    - mode: 0755
    - require:
      - user: {{ fabiolb.user }}
      - group: {{ fabiolb.group }}

fabiolb-link-version:
  file.symlink:
    - name: {{ fabiolb.bin_dir }}/fabio
    - target: {{ fabiolb.dist_dir }}/fabio-{{ fabiolb.version }}
    - user: {{ fabiolb.user }}
    - group: {{ fabiolb.group }}
    - mode: 0755
    - require:
      - user: {{ fabiolb.user }}
      - group: {{ fabiolb.group }}
      - file: fabiolb-install-binary

fabiolb-install-service:
  file.managed:
    - name: /etc/systemd/system/fabiolb.service
    - source: salt://fabiolb/files/fabiolb.service.j2
    - template: jinja
    - context:
        fabiolb: {{ fabiolb }}
  module.run:
    - name: service.systemctl_reload
    - onchanges:
      - file: fabiolb-install-service
